package com.maybank.transaction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transaction.entity.Role;
import com.maybank.transaction.entity.User;

public interface RoleRepository extends JpaRepository<Role, Long> {


}
