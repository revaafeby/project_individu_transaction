package com.maybank.transaction.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transaction.entity.Rekening;

public interface RekeningRepository extends JpaRepository<Rekening, Long> {
	
	Rekening findBynoRekening(String noRekening);
	Optional<Rekening> getRekById(Long id);

}
