package com.maybank.transaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transaction.entity.Transfer;

public interface TransferRepository extends JpaRepository<Transfer, Long>{

}
