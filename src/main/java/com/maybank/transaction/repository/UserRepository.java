package com.maybank.transaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transaction.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByUsername(String username);

}
