package com.maybank.transaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transaction.entity.Provider;

public interface ProviderRepository extends JpaRepository<Provider, Long> {

	Provider findByNamabank(String bankname);
}
