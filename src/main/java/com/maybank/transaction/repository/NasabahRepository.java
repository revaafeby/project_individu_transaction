package com.maybank.transaction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.transaction.entity.Nasabah;

public interface NasabahRepository extends JpaRepository<Nasabah, Long>{

}
