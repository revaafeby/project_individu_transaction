package com.maybank.transaction.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table (name = "nasabah")
public class Nasabah {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
//	@NotBlank
//	@NotEmpty
//	@NotNull
	@Column(name ="nama_lengkap")
	private String namalengkap;
	
	@Column(name ="tgl_lahir")
	private String tgllahir;
	
	@Column(name ="nomor_identitas")
	private String nomoridentitas;
	
	@Column(name ="tipe_identitas")
	private String tipeidentitas;
	
	@Email
	@Column(name ="email")
	private String email;
	
	@Column(name ="kontak")
	private String kontak;
	
//	@Column(name="norek")
//	private String norek;
	
	@OneToMany(mappedBy = "nasabah")
	private List<Rekening> list_rekening;
	
	
	public String getNamalengkap() {
		return namalengkap;
	}
	public void setNamalengkap(String namalengkap) {
		this.namalengkap = namalengkap;
	}
	public String getNomoridentitas() {
		return nomoridentitas;
	}
	public void setNomoridentitas(String nomoridentitas) {
		this.nomoridentitas = nomoridentitas;
	}
	public String getTipeidentitas() {
		return tipeidentitas;
	}
	public void setTipeidentitas(String tipeidentitas) {
		this.tipeidentitas = tipeidentitas;
	}
	public List<Rekening> getList_rekening() {
		return list_rekening;
	}
	public void setList_rekening(List<Rekening> list_rekening) {
		this.list_rekening = list_rekening;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getKontak() {
		return kontak;
	}
	public void setKontak(String kontak) {
		this.kontak = kontak;
	}
	public String getTgllahir() {
		return tgllahir;
	}
	public void setTgllahir(String tgllahir) {
		this.tgllahir = tgllahir;
	}
	

}
