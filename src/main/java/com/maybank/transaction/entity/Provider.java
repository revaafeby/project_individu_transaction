package com.maybank.transaction.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table (name = "provider")
public class Provider {
	
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Long id;
	
	
//	@NotBlank
//	@NotNull
//	@NotEmpty
	@Column(nullable = false, unique = true)
	private String namabank;
	private Number codebank;
	
	@OneToMany(mappedBy = "provider")
	private List<Rekening> list_rekening;
	public String getNamabank() {
		return namabank;
	}
	public void setNamabank(String namabank) {
		this.namabank = namabank;
	}
	public Number getCodebank() {
		return codebank;
	}
	public void setCodebank(Number codebank) {
		this.codebank = codebank;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


}
