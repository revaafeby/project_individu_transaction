package com.maybank.transaction.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transfer")
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Rekening rekPenerimas;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Rekening rekPengirims;
    
    private Date tanggalKirim;
    private double jumlahAmount;
    private double fee;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Rekening getRekPenerimas() {
		return rekPenerimas;
	}
	public void setRekPenerimas(Rekening rekPenerimas) {
		this.rekPenerimas = rekPenerimas;
	}
	public Rekening getRekPengirims() {
		return rekPengirims;
	}
	public void setRekPengirims(Rekening rekPengirims) {
		this.rekPengirims = rekPengirims;
	}
	public Date getTanggalKirim() {
		return tanggalKirim;
	}
	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}
	public double getJumlahAmount() {
		return jumlahAmount;
	}
	public void setJumlahAmount(double jumlahAmount) {
		this.jumlahAmount = jumlahAmount;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	
}

