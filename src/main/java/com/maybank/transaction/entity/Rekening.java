package com.maybank.transaction.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.websocket.OnError;

@Entity
@Table(name = "rekening")
public class Rekening {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;	
//	@NotEmpty
//	@NotBlank
//	@NotNull
//	@Size(min=4, max =10)
	@Column(name="norekening",nullable=false, unique = true)
	private String noRekening;
	private Double saldo = 5000000.0;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "provider_id", referencedColumnName = "id")
	private Provider provider;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "nasabah_id", referencedColumnName = "id")
	private Nasabah nasabah;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rekPengirims", cascade = javax.persistence.CascadeType.ALL)
	private List<Transfer> rekPegirim;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rekPenerimas", cascade = javax.persistence.CascadeType.ALL)
	private List<Transfer> rekPenerima;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Nasabah getNasabah() {
		return nasabah;
	}

	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}

	public List<Transfer> getRekPegirim() {
		return rekPegirim;
	}

	public void setRekPegirim(List<Transfer> rekPegirim) {
		this.rekPegirim = rekPegirim;
	}

	public List<Transfer> getRekPenerima() {
		return rekPenerima;
	}

	public void setRekPenerima(List<Transfer> rekPenerima) {
		this.rekPenerima = rekPenerima;
	}
	
	
	


}
