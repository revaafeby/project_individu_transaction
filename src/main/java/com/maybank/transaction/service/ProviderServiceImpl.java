package com.maybank.transaction.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.repository.ProviderRepository;

@Service
@Transactional
public class ProviderServiceImpl implements ProviderService {
	
	@Autowired
	private ProviderRepository providerRepository;
	
	public List<Provider> getAll(){
		// TODO Auto-generated method stub
			return this.providerRepository.findAll();
			}

			@Override
			public void save(Provider provider) {
				// TODO Auto-generated method stub
				this.providerRepository.save(provider);	
			}

			@Override
			public void delete(Provider provider) {
				// TODO Auto-generated method stub
				this.providerRepository.delete(provider);
				
			}

			@Override
			public Optional<Provider> getProviderById(Long Id) {
				// TODO Auto-generated method stub
				return this.providerRepository.findById(Id);
				
			}
			
 
			}

	

