package com.maybank.transaction.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Role;
import com.maybank.transaction.entity.User;
import com.maybank.transaction.repository.RoleRepository;
import com.maybank.transaction.repository.UserRepository;

@Service
@Transactional
public class InitDefaultUserAuth {
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;
	
	@PostConstruct
	public void index() {
		//create role
      Role roleAdmin = new Role();
      Role roleCS = new Role();
      Role roleOperator = new Role();

      roleAdmin.setRole("admin");
      roleCS.setRole("customerservice");
      roleOperator.setRole("operator");

      
      this.roleRepository.save(roleAdmin);
      this.roleRepository.save(roleCS);
      this.roleRepository.save(roleOperator);

      //create user
      List<Role> listRole = new ArrayList<>();
      List<Role> listRole2 = new ArrayList<>();
      List<Role> listRole3 = new ArrayList<>();
      
//      List<Role> userListRole = new ArrayList<>();
      listRole.add(roleAdmin);
      listRole2.add(roleCS);
      listRole3.add(roleOperator);

      User userAdmin = new User();
      userAdmin.setUsername("admin");
      userAdmin.setEmail("maybank@maybank.co.id");
      userAdmin.setPassword(new BCryptPasswordEncoder().encode("1234"));
      userAdmin.setRoles(listRole);
      
      User userOperator = new User();
      userOperator.setUsername("operator");
      userOperator.setEmail("user@maybank.co.id");
      userOperator.setPassword(new BCryptPasswordEncoder().encode("1234"));
      userOperator.setRoles(listRole3);

      User userCS = new User();
      userCS.setUsername("customerservice");
      userCS.setEmail("cs@maybank.co.id");
      userCS.setPassword(new BCryptPasswordEncoder().encode("1234"));
      userCS.setRoles(listRole2);
      
      this.userRepository.save(userAdmin);
      this.userRepository.save(userOperator);
      this.userRepository.save(userCS);

}
}
