package com.maybank.transaction.service; 

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Provider;

@Service
public interface ProviderService {
	
	public List<Provider> getAll();
	public void save (Provider provider);
	public void delete(Provider provider);
	Optional<Provider> getProviderById(Long Id);

	}

