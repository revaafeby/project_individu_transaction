package com.maybank.transaction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Rekening;

@Service
public interface RekeningService {
	
	public List<Rekening> getAll();
	public void save (Rekening rekening);
	public void delete(Rekening rekening);
	public void deleterekening(Rekening rekening);
	public void getById(Long id);
	public Optional<Rekening> getRekeningById(Long id);
	public Rekening findBynoRekening(String noRekening);
	void delete(Long id);


}
