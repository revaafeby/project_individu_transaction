package com.maybank.transaction.service;

import java.util.List;
import java.util.Optional;


import com.maybank.transaction.entity.Transfer;

public interface TransferService {
	
	public List<Transfer> getAll();
	public void save (Transfer transfer);
	public void delete(Transfer transfer);
	public Optional<Transfer> getTransferById(Long Id);

	}
	
		

