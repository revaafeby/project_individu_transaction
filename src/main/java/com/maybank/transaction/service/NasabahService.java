package com.maybank.transaction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Nasabah;

@Service
public interface NasabahService {
	
	public List<Nasabah> getAll();
	public void save(Nasabah nasabah);
	public void delete(Nasabah nasabah);
	Optional<Nasabah> getNasabahById(Long Id);

	
	
	

}
