package com.maybank.transaction.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.User;
import com.maybank.transaction.repository.RoleRepository;
import com.maybank.transaction.repository.UserRepository;

@Service
public class LoginService {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	public User findUserByUsername(String username) {
		return this.userRepository.findByUsername(username);
	}

	public void addUser(User user) {
		// TODO Auto-generated method stub
		this.userRepository.save(user);
		
	}
	public List<com.maybank.transaction.entity.User> getAll() {
		// TODO Auto-generated method stub
		return this.userRepository.findAll();
	}
	

}
