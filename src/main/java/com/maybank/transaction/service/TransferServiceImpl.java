package com.maybank.transaction.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.entity.Rekening;
import com.maybank.transaction.entity.Transfer;
import com.maybank.transaction.repository.ProviderRepository;
import com.maybank.transaction.repository.RekeningRepository;
import com.maybank.transaction.repository.TransferRepository;

@Service
public class TransferServiceImpl implements TransferService{
	
	@Autowired
	private TransferRepository transferRepository;
	
	@Autowired
	private RekeningRepository rekeningRepository;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Override
	public List<Transfer> getAll(){
		// TODO Auto-generated method stub
			return this.transferRepository.findAll();
			}

			@Override
			public void save(Transfer transfer) {
				// TODO Auto-generated method stub
				Rekening rekeningPengirim = rekeningService.findBynoRekening(transfer.getRekPengirims().getNoRekening());
				Rekening rekeningPenerima = rekeningService.findBynoRekening(transfer.getRekPenerimas().getNoRekening());
				
				Long idPengirim = rekeningPengirim.getId();
				Long idPenerima = rekeningPenerima.getId();
				
				Optional<Rekening> pengirim = this.rekeningService.getRekeningById(idPengirim);
				Optional<Rekening> penerima = this.rekeningService.getRekeningById(idPenerima);
				
				System.out.println(pengirim.get().getNoRekening());
				Rekening rekPengirim = pengirim.get();
				Rekening rekPenerima = penerima.get();
				
				String bankAsal = rekPengirim.getProvider().getNamabank();
				String bankTuju = rekPenerima.getProvider().getNamabank();
				
				Transfer transfers = new Transfer();
				transfers.setTanggalKirim(Date.valueOf(LocalDate.now()));
				transfers.setJumlahAmount(transfer.getJumlahAmount());
				
				if(bankAsal.equals(bankTuju)) {
					transfers.setFee(0);
					rekPengirim.setSaldo(rekPengirim.getSaldo()-(transfer.getJumlahAmount()));
					transfers.setRekPengirims(rekPengirim);
					transfers.setRekPenerimas(rekPenerima);
					rekPenerima.setSaldo(rekPenerima.getSaldo()+(transfer.getJumlahAmount()));
					this.rekeningRepository.save(rekPengirim);
					this.rekeningRepository.save(rekPenerima);
					this.transferRepository.save(transfers);
				}else if(bankAsal != bankTuju) {
					transfers.setFee(6500);
					System.out.println(transfers.getJumlahAmount());
					rekPengirim.setSaldo(rekPengirim.getSaldo()-(transfer.getJumlahAmount()-6500));
					transfers.setRekPengirims(rekPengirim);
					transfers.setRekPenerimas(rekPenerima);
					rekPenerima.setSaldo(rekPenerima.getSaldo()+(transfer.getJumlahAmount()));
					this.rekeningRepository.save(rekPengirim);
					this.rekeningRepository.save(rekPenerima);
					this.transferRepository.save(transfers);
				}
				
				
				
			}

			@Override
			public void delete(Transfer transfer) {
				// TODO Auto-generated method stub
				this.transferRepository.delete(transfer);
				
			}

			@Override
			public Optional<Transfer> getTransferById(Long Id) {
				// TODO Auto-generated method stub
				return this.transferRepository.findById(Id);
				
			}
			
 
			}

