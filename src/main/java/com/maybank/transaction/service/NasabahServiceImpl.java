package com.maybank.transaction.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Nasabah;
import com.maybank.transaction.repository.NasabahRepository;

@Service
@Transactional
public class NasabahServiceImpl implements NasabahService  {
	
	@Autowired
	private NasabahRepository nasabahRepository;
	
	public List<Nasabah> getAll(){
		// TODO Auto-generated method stub
				return this.nasabahRepository.findAll();
			}

			@Override
			public void save(Nasabah nasabah) {
				// TODO Auto-generated method stub
				this.nasabahRepository.save(nasabah);	
			}

			@Override
			public void delete(Nasabah nasabah) {
				// TODO Auto-generated method stub
				this.nasabahRepository.delete(nasabah);
				
			}

			@Override
			public Optional<Nasabah> getNasabahById(Long Id) {
				// TODO Auto-generated method stub
				return this.nasabahRepository.findById(Id);
				
			}


	}

