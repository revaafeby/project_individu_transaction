package com.maybank.transaction.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.transaction.entity.Rekening;
import com.maybank.transaction.repository.RekeningRepository;

@Service
public class RekeningServiceImpl implements RekeningService{
	
	@Autowired
	private RekeningRepository rekeningRepository;
	
	@Override
	public List<Rekening> getAll(){
		// TODO Auto-generated method stub
			return this.rekeningRepository.findAll();
			}

			@Override
			public void save(Rekening rekening) {
				// TODO Auto-generated method stub
				this.rekeningRepository.save(rekening);	
			}

			@Override
			public void delete(Long id) {
				// TODO Auto-generated method stub
				this.rekeningRepository.deleteById(id);
			}
			
			@Override
			public void getById(Long id) {
				//TODO Auto-generated method stub
				this.rekeningRepository.findById(id);
			}

			@Override
			public Rekening findBynoRekening(String noRekening) {
				// TODO Auto-generated method stub
				return this.rekeningRepository.findBynoRekening(noRekening);

			}

			@Override
			public Optional<Rekening> getRekeningById(Long Id) {
				// TODO Auto-generated method stub
				return this.rekeningRepository.findById(Id);
			}

			@Override
			public void deleterekening(Rekening rekening) {
				// TODO Auto-generated method stub
			this.rekeningRepository.delete(rekening);	
			}

			@Override
			public void delete(Rekening rekening) {
				// TODO Auto-generated method stub
				
			}

}
