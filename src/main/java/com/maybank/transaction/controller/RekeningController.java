package com.maybank.transaction.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.entity.Rekening;
import com.maybank.transaction.service.RekeningService;

@Controller
@RequestMapping("/rekening")
public class RekeningController {
	
	@Autowired
	private RekeningService rekeningService;
	
	@GetMapping
	public String index (String keyword, Model model) {
		List<Rekening> rekening= this.rekeningService.getAll();
		model.addAttribute("rekeningForm", new Rekening());
		model.addAttribute("rekening", rekening);
		return "rekening";
	}
	
	@PostMapping("/save")
	public String save(Rekening rekening, RedirectAttributes redirectAttributes) {
		System.out.println("Bank Name :"+ rekening.getNoRekening());
		this.rekeningService.save(rekening);
		redirectAttributes.addFlashAttribute("succes", "data inserted");
		return "redirect:/rekening";
	}
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long Id) {
		Optional<Rekening> rekening = this.rekeningService.getRekeningById(Id);
		if(rekening.isPresent()) {
			this.rekeningService.deleterekening(rekening.get());
	}
		return "redirect:/rekening";
	}

}
