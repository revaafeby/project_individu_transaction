package com.maybank.transaction.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.transaction.entity.Nasabah;
import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.entity.Rekening;
import com.maybank.transaction.entity.Transfer;
import com.maybank.transaction.repository.RekeningRepository;
import com.maybank.transaction.service.NasabahService;
import com.maybank.transaction.service.ProviderService;
import com.maybank.transaction.service.RekeningService;
import com.maybank.transaction.service.TransferService;

import net.bytebuddy.asm.Advice.This;

@Controller
@RequestMapping("/transfer")
public class TransferController {
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private NasabahService nasabahService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private TransferService transferService;
	
	@Autowired
	private RekeningRepository rekeningRepository;

	
	@GetMapping
	public String index(Model model) {
		List<Provider> providers = this.providerService.getAll();
		List<Rekening> rekenings =this.rekeningService.getAll();
		List<Transfer> transfers = this.transferService.getAll();
		
		model.addAttribute("transfers", transfers);
		model.addAttribute("rekenings", rekenings);
		model.addAttribute("providers", providers);
		
		model.addAttribute("transferForm", new Transfer());
		model.addAttribute("rekeningForm", new Rekening());
		model.addAttribute("providerForm", new Provider());
		model.addAttribute("nasabahForm", new Nasabah());
		return "transfer";
		
	}
	@PostMapping("/save")
	public String save(
			@ModelAttribute("transferForm") Transfer transfer,
			Model model) {
		List<Rekening> rekenings = this.rekeningService.getAll();
		List<Transfer> transfers = this.transferService.getAll();
		Rekening rekeningPengirim = rekeningService.findBynoRekening(transfer.getRekPengirims().getNoRekening());
		Rekening rekeningPenerima = rekeningService.findBynoRekening(transfer.getRekPenerimas().getNoRekening());
		Long idPengirim = rekeningPengirim.getId();
		Long idPenerima = rekeningPenerima.getId();
		
		Optional<Rekening> pengirim = this.rekeningService.getRekeningById(idPengirim);
		Optional<Rekening> penerima = this.rekeningService.getRekeningById(idPenerima);
		
		String bankAsal = rekeningPengirim.getProvider().getNamabank();
		String bankTuju = rekeningPenerima.getProvider().getNamabank();
		
		
		double saldo;
		if(bankAsal.equals(bankTuju)) {
			saldo = rekeningPengirim.getSaldo()- transfer.getJumlahAmount();
			if(saldo<50000) { 
				model.addAttribute("transfers", transfers);
				return "redirect:/transfer";
			} 
			else {
				this.transferService.save(transfer);
				return "redirect:/transfer";
			}
			
			
		}else if(bankAsal != bankTuju) {
			saldo = rekeningPengirim.getSaldo()- transfer.getJumlahAmount() - 6500;
			if(saldo<50000) { 
				model.addAttribute("transfers", transfers);
				return "redirect:/transfer";
			} 
			else {
				this.transferService.save(transfer);
				return "redirect:/transfer";
			}
		}
		
		
		
		this.transferService.save(transfer);
		return "redirect:/transfer";
	}

}
