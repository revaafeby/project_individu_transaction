package com.maybank.transaction.controller;

import java.util.List;
import java.util.Optional;

import org.aspectj.apache.bcel.classfile.Module.Provide;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.transaction.entity.Nasabah;
import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.entity.Rekening;
import com.maybank.transaction.service.NasabahService;
import com.maybank.transaction.service.ProviderService;
import com.maybank.transaction.service.RekeningService;

@Controller
@RequestMapping("/nasabah")
public class NasabahController {
	
//	@Autowired
//	private NasabahService nasabahService;
//	@GetMapping
//	public String index (String keyword, Model model) {
//		List<Nasabah> nasabah = this.nasabahService.getAll();
//		model.addAttribute("nasabahForm", new Nasabah());
//		model.addAttribute("nasabah", nasabah);
//		return "nasabah";
//	}
//	
//	@PostMapping("/save")
//	public String save(Nasabah nasabah, RedirectAttributes redirectAttributes) {
//		System.out.println("Employee Name :"+ nasabah.getNamalengkap());
//		this.nasabahService.save(nasabah);
//		redirectAttributes.addFlashAttribute("succes", "data inserted");
//		return "redirect:/nasabah";
//	}
//	@GetMapping("/delete")
//	public String delete(@RequestParam("id") Long Id) {
//		Optional<Nasabah> nasabah = this.nasabahService.getNasabahById(Id);
//		if(nasabah.isPresent()) {
//			this.nasabahService.delete(nasabah.get());
//	}
//		return "redirect:/nasabah";
//	}
	
	@Autowired
    private RekeningService rekeningService;

    @Autowired
    private NasabahService nasabahService;

    @Autowired
    private ProviderService providerService;

    @GetMapping
    public String index(Model model) {
        List<Provider> providers = this.providerService.getAll(); 
        List<Rekening> rekening = this.rekeningService.getAll(); 

        model.addAttribute("rekening",rekening);
        model.addAttribute("providers", providers);

        model.addAttribute("rekeningForm",new Rekening());
        model.addAttribute("nasabahForm",new Nasabah());
        return "nasabah";
    }

    @PostMapping("/save")
    public String save(
            @ModelAttribute("nasabahForm")Nasabah nasabah,
            @ModelAttribute("rekeningForm")Rekening rekening,
            @ModelAttribute("providers")Provider provider, Model model){

    	this.nasabahService.save(nasabah);
        rekening.setSaldo(1000000.0);
        rekening.setNasabah(nasabah);
        rekening.setProvider(provider);
        this.rekeningService.save(rekening);
        

        return "redirect:/nasabah";
        }
    }


