package com.maybank.transaction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.repository.NasabahRepository;
import com.maybank.transaction.repository.ProviderRepository;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	private NasabahRepository nasabahRepository;
	
	@Autowired
	private ProviderRepository providerRepository;
	
	@GetMapping
	public String nasabahPage(Model mdl) {
		List<Provider> provider = this.providerRepository.findAll();
		mdl.addAttribute("provider", provider);
		
		return "home";
		
		
	}

}
