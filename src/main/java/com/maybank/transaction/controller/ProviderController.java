package com.maybank.transaction.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.transaction.entity.Nasabah;
import com.maybank.transaction.entity.Provider;
import com.maybank.transaction.entity.Rekening;
import com.maybank.transaction.service.NasabahService;
import com.maybank.transaction.service.ProviderService;
import com.maybank.transaction.service.RekeningService;

@Controller
@RequestMapping("/provider")
public class ProviderController {
	
	@Autowired
	private NasabahService nasabahService;
	@Autowired
	private RekeningService rekeningService;
	@Autowired
	private ProviderService providerService;
	
	@GetMapping
	public String index (String keyword, Model model) {
		List<Provider> providers = this.providerService.getAll();
		model.addAttribute("providerForm", new Provider());
		model.addAttribute("provider", providers);
		return "provider";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("providerForm") Provider provider,
		Model model, BindingResult result){
		if(result.hasErrors()) {
			return "provider";
		}
		
		this.providerService.save(provider);
		return "redirect:/provider";
	}
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long Id) {
		Optional<Provider> provider = this.providerService.getProviderById(Id);
		if(provider.isPresent()) {
			this.providerService.delete(provider.get());
	}
		
		return "redirect:/provider";
	}
	
}
